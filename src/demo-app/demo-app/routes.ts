import {Routes} from '@angular/router';
import {Home} from './demo-app';
import {ListDemo} from '../list/list-demo';

export const DEMO_APP_ROUTES: Routes = [
  {path: '', component: Home},
  {path: 'list', component: ListDemo}
];
